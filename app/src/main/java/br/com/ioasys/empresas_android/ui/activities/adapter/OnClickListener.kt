package br.com.ioasys.empresas_android.ui.activities.adapter

import android.view.View

interface OnClickListener {
    fun onItemClickListener (view : View, position : Int)
}