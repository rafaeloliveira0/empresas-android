package br.com.ioasys.empresas_android.ui.activities.activies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import br.com.ioasys.empresas_android.R
import br.com.ioasys.empresas_android.ui.activities.adapter.EmpresaAdapter

class MainActivity : AppCompatActivity() {

    var toolbarSearch : Toolbar? = null
    var searchView  : SearchView? = null
    var logoToolbar : ImageView? = null
    var textMain : TextView? = null
    var linearRecycleView: LinearLayout? = null

    lateinit var empresaAdapter : EmpresaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbarSearch = findViewById(R.id.toolbarSearch)
        searchView = findViewById(R.id.searchView)
        textMain = findViewById(R.id.textMain)
        linearRecycleView = findViewById(R.id.linearRecycleView)

        searchView!!.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    logoToolbar!!.visibility = View.GONE
                    textMain!!.visibility = View.GONE
                    linearRecycleView!!.visibility = View.VISIBLE
                }
            })

            searchView!!.setOnCloseListener(object : SearchView.OnCloseListener {
                override fun onClose(): Boolean {
                    logoToolbar!!.visibility = View.VISIBLE
                    textMain!!.visibility = View.VISIBLE
                    linearRecycleView!!.visibility = View.GONE

                    return false
                }
            })

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean = false

                override fun onQueryTextChange(newText: String?): Boolean {
                    empresaAdapter.filter.filter(newText)
                    return false
                }

            })
    }

}
