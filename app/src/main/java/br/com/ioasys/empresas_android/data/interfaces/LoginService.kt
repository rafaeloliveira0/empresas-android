package br.com.ioasys.empresas_android.data.interfaces

import br.com.ioasys.empresas_android.data.Login
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call;
import retrofit2.http.*

interface LoginService {

    @POST("users/auth/sign_in")
    @FormUrlEncoded
    abstract fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<Login>

    @GET("enterprises/")
    abstract fun consultar(
        @Query("access-token") access_token: String,
        @Query("client") client: String,
        @Query("uid") uid: String
    ): Call<JsonObject>
}