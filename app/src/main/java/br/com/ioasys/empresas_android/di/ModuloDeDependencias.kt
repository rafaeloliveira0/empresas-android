package br.com.ioasys.empresas_android.di

import br.com.ioasys.empresas_android.data.RetrofitConfig
import br.com.ioasys.empresas_android.data.repository.LoginRepository
import br.com.ioasys.empresas_android.viewmodels.LoginViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

object ModuloDeDependencias {
    val networkModule = module {
        single {RetrofitConfig()}
    }

    val repositoryModule = module {
        single<LoginRepository> { LoginRepository() }
    }

    val viewModelModule = module{
        viewModel{LoginViewModel(get())}
    }

}