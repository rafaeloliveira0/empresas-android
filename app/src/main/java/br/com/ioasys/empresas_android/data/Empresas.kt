package br.com.ioasys.empresas_android.data

import java.io.Serializable

data class Empresas (
     var enterprise_name: String? = null,
     var description: String? = null,
     var email_enterprise: String? = null,
     var own_enterprise: Boolean = false,
     var photo: String? = null,
     var country: String? = null,
     var enterprise_type: TypeEnterprise? = null
) : Serializable