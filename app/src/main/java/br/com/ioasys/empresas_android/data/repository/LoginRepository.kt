package br.com.ioasys.empresas_android.data.repository

import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.lifecycle.MutableLiveData
import br.com.ioasys.empresas_android.data.Login
import retrofit2.Call
import br.com.ioasys.empresas_android.data.RetrofitConfig
import br.com.ioasys.empresas_android.data.User
import br.com.ioasys.empresas_android.ui.activities.activies.MainActivity
import com.google.gson.JsonObject
import com.squareup.moshi.Json
import retrofit2.Callback
import retrofit2.Response


class LoginRepository() {

    private var loginSuccess : MutableLiveData<Boolean> = MutableLiveData()
    /*private var userPreferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences()*/

    fun login(email: String, password:String) : MutableLiveData<Boolean> {

        val call: Call<Login> = RetrofitConfig().getLoginService().login(email, password)
        call.enqueue(object : Callback<Login> {
            override fun onFailure(call: Call<Login>, t: Throwable) {
                loginSuccess.value = false
            }

            override fun onResponse(call: Call<Login>, response: Response<Login>) {
                if (response.isSuccessful) {
                    /*var prefsEditor = userPreferences.edit()
                    prefsEditor.putString("access-tokes", response.headers().get("access-token"))
                    prefsEditor.putString("client" , response.headers().get("client"))
                    prefsEditor.putString("uid", response.headers().get("uid"))
                    prefsEditor.apply()*/
                    User.accesstoken = response.headers().get("access-token")
                    User.client = response.headers().get("client")
                    User.uid = response.headers().get("uid")
                    loginSuccess.value = true

                } else {
                    loginSuccess.value = false
                }
            }
        })
        return loginSuccess
    }
}