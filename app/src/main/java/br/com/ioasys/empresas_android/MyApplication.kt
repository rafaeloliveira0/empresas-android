package br.com.ioasys.empresas_android

import android.app.Application
import br.com.ioasys.empresas_android.di.ModuloDeDependencias
import org.koin.android.ext.android.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this,
            listOf(
                ModuloDeDependencias.viewModelModule,
                ModuloDeDependencias.networkModule,
                ModuloDeDependencias.repositoryModule
            ))
    }
}