package br.com.ioasys.empresas_android.data

import com.google.gson.annotations.SerializedName

class Login {

    @SerializedName("email")
    var email: String? = null

    @SerializedName("password")
    var password: String? = null
}