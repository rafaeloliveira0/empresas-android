package br.com.ioasys.empresas_android.data

import java.io.Serializable

data class TypeEnterprise (
    val id: Int?,
    val enterprise_type_name: String? = ""
) : Serializable