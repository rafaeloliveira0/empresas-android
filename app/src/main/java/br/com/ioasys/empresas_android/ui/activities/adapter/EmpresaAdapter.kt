package br.com.ioasys.empresas_android.ui.activities.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import br.com.ioasys.empresas_android.R
import br.com.ioasys.empresas_android.data.Empresas
import com.bumptech.glide.Glide
import java.util.stream.Collectors

class EmpresaAdapter(contexto : Context, empresa : List<Empresas>): RecyclerView.Adapter<EmpresaAdapter.ItemView>() , Filterable  {

    var empresasLista : List<Empresas> = empresa
    var empresaCompleta : List<Empresas> = empresasLista
    var context : Context = contexto
    var empresaListaBoolean : Boolean = false
    var layoutInflater = LayoutInflater.from(context)


    class ItemView(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var click : OnClickListener? = null
        var imagemEmpresa: ImageView? = itemView.findViewById(R.id.imagemEmpresa)
        var textoNomeEmpresa: TextView? = itemView.findViewById(R.id.textNomeEmpresa)
        var textTipoNegocio: TextView? = itemView.findViewById(R.id.textTipoNegocio)
        var textPais: TextView? = itemView.findViewById(R.id.textPais)
        val cardEmpresa: CardView? = itemView.findViewById(R.id.cardViewDescription)

        override fun onClick(v: View?) {
            if(click != null ){
                click!!.onItemClickListener(v!!, getAdapterPosition())
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmpresaAdapter.ItemView {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.linha_empresa, parent, false)
        val itemViewHolder = ItemView(view)
        return itemViewHolder
    }

    override fun getItemCount(): Int = empresasLista.size

    override fun onBindViewHolder(holder: EmpresaAdapter.ItemView, position: Int) {
        Glide.with(context)
            .load("http://empresas.ioasys.com.br" + empresasLista.get(position).photo)
            .fitCenter()
            .placeholder(R.drawable.img_e_1)
            .into(holder.imagemEmpresa)
        holder.textoNomeEmpresa!!.text(empresasLista.get(position).enterprise_name)
        holder.textTipoNegocio!!.text(empresasLista.get(position).enterprise_type!!.enterprise_type_name)
        holder.textPais!!.text(empresasLista.get(position).country)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                var retornoBusca : List<Empresas>
                var filterResults : FilterResults? = null
                val constraint = constraint.toString().toLowerCase()
                if(!constraint.equals("")){
                     retornoBusca = empresasLista.stream().filter { e ->
                         e.enterprise_name!!.contains(constraint)
                     }.collect(Collectors.toList())
                    filterResults!!.count = retornoBusca.size
                    filterResults!!.values = retornoBusca
                    return filterResults
                    }else{
                    empresaListaBoolean = true
                    return filterResults!!
                }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if(empresaListaBoolean){
                    empresasLista = empresaCompleta
                    notifyDataSetChanged()
                    empresaListaBoolean = false
                }else{
                    empresasLista = results!!.values as List<Empresas>
                }
            }

        }
    }

}



private operator fun Any.invoke(enterprise_name: String?) {

}
