package br.com.ioasys.empresas_android.data

import br.com.ioasys.empresas_android.data.interfaces.LoginService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfig{
    fun getLoginService() : LoginService {
        val retrofitConfig = Retrofit.Builder()
            .baseUrl("http://empresas.ioasys.com.br/api/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofitConfig.create(LoginService::class.java)
    }
}