package br.com.ioasys.empresas_android.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.ioasys.empresas_android.data.repository.LoginRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

class LoginViewModel (private val repository: LoginRepository) : ViewModel(){


    fun login(email:String, password:String) : MutableLiveData<Boolean>{
       return repository.login(email, password)
    }
}