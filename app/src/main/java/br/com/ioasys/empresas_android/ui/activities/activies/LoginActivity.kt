package br.com.ioasys.empresas_android.ui.activities.activies

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import br.com.ioasys.empresas_android.R
import br.com.ioasys.empresas_android.viewmodels.LoginViewModel
import org.koin.android.ext.android.inject

open class LoginActivity : AppCompatActivity() {

    var email: EditText? = null
    var password: EditText? = null
    var btnEntrar: Button? = null
    var progressBar: ProgressBar? = null
    var isSuccess = false

    val loginViewModel: LoginViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        email = findViewById(R.id.textEmail)
        password = findViewById(R.id.textPassword)
        progressBar = findViewById(R.id.progressBar)
        btnEntrar = findViewById(R.id.buttonEntrar)

        btnEntrar!!.setOnClickListener{
            if(validarFormulario()){
                progressBar!!.visibility = View.VISIBLE
                btnEntrar!!.isEnabled = false
                email!!.isEnabled = false
                password!!.isEnabled = false

                loginViewModel.login(email!!.text.toString(), password!!.text.toString())?.observe(this,
                    Observer { data  -> data?.let {
                        if(it!!){
                            val intent = Intent(this@LoginActivity, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else{
                            Toast.makeText(applicationContext, "Usuário ou senha incorreto!", Toast.LENGTH_LONG).show()
                        }
                    } })
            }
        }
    }

    private fun validarFormulario(): Boolean {
        var valido: Boolean? = true
        var foco: Boolean? = false

        if(email!!.getText().toString().replace(" ","").isEmpty()
                || !email!!.getText().toString().contains("@")
                || !email!!.getText().toString().contains(".")){
            email!!.getText().clear()
            email!!.error = "E-mail é obrigatório"

            valido = false
            if((!foco!!)){
                email!!.requestFocus()
                foco = true
            }
        }

        if(password!!.getText().toString().replace(" ","").isEmpty()){
            password!!.getText().clear()
            password!!.error = "Senha é obrigatório"

            valido = false
            if((!foco!!)){
                password!!.requestFocus()
                foco = true
            }
        }
        return valido!!
    }

    override fun onResume() {
        super.onResume()
        email!!.error = null
        email!!.text.clear()
        password!!.error = null
        password!!.text.clear()
    }
}